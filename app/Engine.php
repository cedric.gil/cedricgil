<?php

/**
 * Class Engine
 */
class Engine
{

    /**
     * Injections des dépendences
     */
    private static function Auth(){
        return Auth::getInstance(Database::getInstance(Config::getInstance()));
    }
    private static function Database(){
        return Database::getInstance(Config::getInstance());
    }
    private static function Session(){
        return Session::getInstance();
    }
    private static function Twig(){
        return Twig::getInstance();
    }

    /**
     * @param string $page
     * @param string $spage
     * @param array $paramsfromphpfile
     * Retourne le rendu Twig
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public static function TwigRender($page, $spage, $paramsfromphpfile){
        return self::Twig()->getRender($page, $spage, $paramsfromphpfile);
    }

    /**
     * @param string $page
     * Retourne le chemin du fichier PHP lié à la page
     * @return bool|string
     */
    public static function TwigPhpPath($page, $spage){
        return self::Twig()->getPhpPath($page, $spage);
    }

}

?>